#include "pch.h"
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <math.h>
#include <limits>
#include "Call.h"

using namespace std;

Call::Call()
{
	
}

Call::~Call()
{

}
void Call::Program1()
{
	int a;
	a = 5;
	cout << a << endl;

	if (ValidateInput(stdInput,true) == 'e'){ return;}

	int b;
	b = 3;
	cout << a << " and " << b << endl;

	if (ValidateInput(stdInput, true) == 'e') { return;}

	int c;
	c = a + b;
	cout << c << endl;

	if (ValidateInput(stdInput, true) == 'e') { return; }
}
void Call::Program2()
{
	float a;
	a = 3.3;
	cout << a << endl;

	if (ValidateInput(stdInput, true) == 'e') { return; }

	int b;
	b = a; //The integer gets truncated to 3;
	cout << "Integer: " << b << endl;
	cout << "Integer/Float: " << b / a << endl;
	cout << "Float/Integer: " << a / b << endl;
	//the integer is implicitly converted into a float as to not loose data

	if (ValidateInput(stdInput, true) == 'e') { return; }
}
void Call::Program3()
{
	float a = 0.1f;
	float b = 0.2f;
    cout << a + b << endl;

	if (ValidateInput(stdInput, true) == 'e') { return; }

	cout.precision(8);
	cout << a + b << endl;

	//precicion describes the desired length of a float point value (output)F.ex  float f = 3.1234 cout.precision(2) would result in 3.12
	//as to why there is an added 0.00000001 to the result might be because writing 0.30000000 is equal to 0.3
	if (ValidateInput(stdInput, true) == 'e') { return; }
}
void Call::Program4()
{
	cout << "1/2 = " << 1 / 2 << endl;
	if (ValidateInput(stdInput, true) == 'e') { return; }
	cout << "1.0f/2 = " << 1.0f / 2 << endl;
	if (ValidateInput(stdInput, true) == 'e') { return; }
	cout << "1/2.0f = " << 1 / 2.0f << endl;
	if (ValidateInput(stdInput, true) == 'e') { return; }

	//And integer can't contain decimal values, therefore the 0.5 is truncated to 0 
	//But as there is a declared float in the other two examples the integer gets implicitly converted to a float value.

}
void Call::Program5()
{

	long a = 1;
	for (size_t i = 0; i < 16; i++)
	{
		a *= 2;
		cout << a << endl;
		
	}
	if (ValidateInput(stdInput, true) == 'e') { return; }
}
void Call::Program6()
{
	//Like it's abviously gonna throw an exception if i try to square a string, but as ill be using cin its just gonna square zero..
	//If i don't of course make sure that the user inputs an actual value, but that would ruin this excersise

	int i_userInput = 0;
	bool validInput = false;

	do
	{
		try {
			
			cout << "Please input an integer" << endl;
			cin.exceptions(ifstream::failbit);
			cin >> i_userInput;
			validInput = true;
		}
		catch(ios_base::failure&fail)
		{
			cout << "Invalid input" << endl;
		}
		cin.clear();//Credit https://stackoverflow.com/questions/7413247/cin-clear-doesnt-reset-cin-object
		cin.ignore(numeric_limits<streamsize>::max(), '\n');//So apparently cin ignores all input when going into the error state so it just loops forever, i tried to input this into the try body but nothing happened.
	
	}while (!validInput);

	cout << "That value squared is: " << pow(i_userInput, 2) << endl;
	if (ValidateInput(stdInput, true) == 'e') { return; }
	validInput = false;

	float f_userInput = 0;
	do
	{
		try {
			cout << "Please input a float" << endl;
			cin.exceptions(ifstream::failbit);
			cin >> f_userInput;
			validInput = true;
		}
		catch (ios_base::failure&fail)
		{
			cout << "Invalid input" << endl;
		}
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');

	} while (!validInput);

	cout << "That value squared is: " << pow(f_userInput, 2) << endl;
	if (ValidateInput(stdInput, true) == 'e') { return; }


}
void Call::Program7()
{
	float a = CheckIfFloat();
	float b = CheckIfFloat();

	cout << "The calculated area is: " << a * b << endl;
	if (ValidateInput(stdInput, true) == 'e') { return; }
}
void Call::Program8()
{
	cout << "Sizeof(bool) : " << sizeof(bool) << endl;
	cout << "Sizeof(char) : " << sizeof(char) << endl;
	cout << "Sizeof(short) : " << sizeof(short) << endl;
	cout << "Sizeof(int) : " << sizeof(int) << endl;
	cout << "Sizeof(long long) : " << sizeof(long long) << endl;
	cout << "Sizeof(float) : " << sizeof(float) << endl;
	cout << "Sizeof(double) : " << sizeof(double) << endl;
	if (ValidateInput(stdInput, true) == 'e') { return; }
	//returns the size of what's inside of the parameters in bytes
}
void Call::Program9()
{
	float a = CheckIfFloat();
	float b = CheckIfFloat();

	float largest = (a > b) ? a : (b > a) ? b : NULL;

	if(largest == NULL)
	{
		cout << "The values are identical!" << endl;
	}
	else
	{
		cout << "The largest of the two is: " << largest << endl;
	}
	if (ValidateInput(stdInput, true) == 'e') { return; }	

}
void Call::Program10()
{
	cout << "(How many bottles should be counted?)" << endl;
	float a = CheckIfFloat();
	for (size_t i = a; a > 0; a--)
	{
		cout << a << " bottles of juice on the wall, one fell down" << endl;
	}
	cout << "No bottles left on the wall!" << endl;
	if (ValidateInput(stdInput, true) == 'e') { return; }
}
void Call::Program11()
{
	float value = CheckIfFloat();

	if(value > 0)
	{
		float value = 10;
		value *= 2;
	}
	else if(value == 0)
	{
		cout << "Zero is not a valid number!" << endl;
	}
	else
	{
		cout << "Value: " << value << endl;
	}
	if (ValidateInput(stdInput, true) == 'e') { return; }

	//What does the program output? 
	//If you type something larger than 0, absolutely nothing, else if you put zero you'll get an error message and anything below 0 will just spit out your number back.
}
void Call::Program12()
{
	for (size_t i = 0; i < 11; i++)
	{
		cout << i << endl;
		cout << 10 - i << endl;
	}
	if (ValidateInput(stdInput, true) == 'e') { return; }
}
void Call::Program13()
{
	cout << "These numbers between 0-100 are devisable by 3" << endl;

	for (size_t i = 1; i < 100; i++)
	{
		if(i%3 == 0)
		{
			cout << i << endl;
		}
	}
	if (ValidateInput(stdInput, true) == 'e') { return; }
}
void Call::Program14()
{
	for (size_t i = 0; i < 8; i++)
	{
		cout << i << endl;
	}
	cout << "Going down." << endl;
	for (size_t i = 8; i > 0; i--)
	{
		cout << i-1 << endl;
	}
	if (ValidateInput(stdInput, true) == 'e') { return; }
}
void Call::Program15()
{
	cout << "Input a positive integer to be factorized: " << endl;
	int a = 0;
	int result = 1;
	while(a <= 0)
	{
		a = CheckIfInt();
		if (a > 0) { break; }
		cout << "Please input a value larger than 0" << endl;
	}
	for (size_t i = 1; i < a+1; i++)
	{
		result *= i;
	}
	cout << result << endl;
	if (ValidateInput(stdInput, true) == 'e') { return; }
}
void Call::Program16()
{

}
float Call::CheckIfFloat()
{
	bool validInput = false;
	float f_userInput = 0;
	do
	{
		try {
			cout << "Please input a float" << endl;
			cin.exceptions(ifstream::failbit);
			cin >> f_userInput;
			validInput = true;
		}
		catch (ios_base::failure&fail)
		{
			cout << "Invalid input" << endl;
		}
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');

	} while (!validInput);

	return f_userInput;
}
int Call::CheckIfInt()
{
	bool validInput = false;
	int i_userInput = 0;
	do
	{
		try {
			cout << "Please input an integer" << endl;
			cin.exceptions(ifstream::failbit);
			cin >> i_userInput;
			validInput = true;
		}
		catch (ios_base::failure&fail)
		{
			cout << "Invalid input" << endl;
		}
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');

	} while (!validInput);

	return i_userInput;
}
//this is doubling as a way to wait for the users input(im a bit lazy) 
char Call::ValidateInput(vector<char>& validInputs, bool state)
{
	char userInput;
	bool validInput = false;

	if(state)
	{
		string iOne;
		string iTwo;
		stringstream ss;

		ss << validInputs[0] << endl; //Always flush the stream or you can't re-use it
		ss >> iOne;
		ss << validInputs[1] << endl;
		ss >> iTwo;
		//So uh apparently you can't just implicitly cast chars into strings hence the code above.... There must be a better way to do that


		cout << "Input: " + iOne << " to proceed or: " + iTwo << " to go back!" << endl;
	}
	else if(!state)
	{
		cout << "Please input your selection according to the choices displayed:" << endl;
	}
	do
	{
		try
		{
			cin >> userInput;
			userInput = tolower(userInput);//Just so that capslock doesn't throw you off;
			for (char input : validInputs) { if (userInput == input) { validInput = true; break; } }
			if (!validInput) { throw 0; }
		}
		catch (int i)
		{
			cout << "Invalid input!" << endl;
		}
	} while (!validInput);

	return userInput;
}
