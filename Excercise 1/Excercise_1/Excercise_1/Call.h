#pragma once
#include <vector>
using namespace std;
class Call
{

public:
	void Program1();
	void Program2();
	void Program3();
	void Program4();
	void Program5();
	void Program6();
	void Program7();
	void Program8();
	void Program9();
	void Program10();
	void Program11();
	void Program12();
	void Program13();
	void Program14();
	void Program15();
	void Program16();
	Call();
	
	~Call();

private:


	char ValidateInput(vector<char>& validInputs, bool state); //state TRUE is either negative or positive while state FALSE might take multiple inputs
	float CheckIfFloat();
	int CheckIfInt();

protected:
	vector<char> stdInput = { 'q','e' };
	
};

